#ifndef PENDINGTASKS_H
#define PENDINGTASKS_H

#include <QDialog>

#include <QCoroTask>

namespace Ui {
class PendingTasks;
}

class OvhApi;
class QTimer;

class PendingTasks : public QDialog
{
    Q_OBJECT

public:
    explicit PendingTasks(OvhApi *api, const QString &path, QWidget *parent = nullptr);
    ~PendingTasks();

private slots:
    QCoro::Task<> refresh();

private:
    Ui::PendingTasks *ui;
    OvhApi *api;
    QTimer *refreshTimer;
    QString apiPath;
    QString baseRefreshText;
};

#endif // PENDINGTASKS_H
