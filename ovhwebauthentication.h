#ifndef OVHWEBAUTHENTICATION_H
#define OVHWEBAUTHENTICATION_H

#include <QDialog>
#include <QCoroTask>

namespace Ui {
class OvhWebAuthentication;
}

class OvhWebAuthentication : public QDialog
{
    Q_OBJECT

public:
    explicit OvhWebAuthentication(QWidget *parent = nullptr);
    ~OvhWebAuthentication();

    QCoro::Task<QByteArray> download(const QUrl &url);

signals:
    void authenticated();
    void webEngineDownloadFinished(const QString &dlFileName);

private:
    Ui::OvhWebAuthentication *ui;
};

#endif // OVHWEBAUTHENTICATION_H
