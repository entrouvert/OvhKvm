#include "dedicatedservergroupwidget.h"
#include "ui_dedicatedservergroupwidget.h"
#include <QTimer>
#include <QJsonDocument>
#include <QJsonObject>
#include "ovhapi.h"


// TODO : unify
QString _monitoringWithIntervention, _monitoringWithoutIntervention, _noMonitoring;

DedicatedServerGroupWidget::DedicatedServerGroupWidget(OvhApi *api, const QString &groupName, const QStringList &servers, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DedicatedServerGroupWidget),
    api(api),
    _groupName(groupName),
    _servers(servers),
    totalMonthlyCost(0),
    incompletePricing(false)
{
    ui->setupUi(this);

    ui->serverTable->hideColumn(0);
    ui->serverTable->setRowCount(servers.count());
    connect(this, &DedicatedServerGroupWidget::requestSingleServerData, this, &DedicatedServerGroupWidget::fetchSingleServerData);

    if (_monitoringWithIntervention.isEmpty()) {
        _monitoringWithIntervention = tr("Enabled with intervention");
        _monitoringWithoutIntervention = tr("Enabled without intervention");
        _noMonitoring = tr("Disabled");
    }

    fetchOvhData();
}

DedicatedServerGroupWidget::~DedicatedServerGroupWidget()
{
    delete ui;
}

void DedicatedServerGroupWidget::fetchOvhData()
{
    ui->groupLabel->setText(tr("Group %1").arg(_groupName));
    int i = 0;
    for (auto &&server: _servers) {
        emit requestSingleServerData(i, server);
        i++;
    }
}

QCoro::Task<> DedicatedServerGroupWidget::fetchSingleServerData(int i, QString server)
{
    qDebug() << "in fetch single";
    qDebug() << "Fetching for row " << i;
    int c = 0;
    qDebug() << server;

    auto serverInfo = (co_await api->get("/dedicated/server/" + server, 1h)).object();

    auto tableItem = new QTableWidgetItem(server);
    ui->serverTable->setItem(i, c++, tableItem);
    tableItem = new QTableWidgetItem(serverInfo["reverse"].toString());
    ui->serverTable->setItem(i, c++, tableItem);

    auto networkInfo = (co_await api->get(QString("/dedicated/server/%1/specifications/network").arg(server), 240h)).object();
    auto routingInfo = networkInfo["routing"].toObject();
    auto v4Info = routingInfo["ipv4"].toObject();
    tableItem = new QTableWidgetItem(v4Info["ip"].toString());
    ui->serverTable->setItem(i, c++, tableItem);


    if (serverInfo["monitoring"].toBool()) {
        if (serverInfo["noIntervention"].toBool())
            tableItem = new QTableWidgetItem(_monitoringWithoutIntervention);
        else
            tableItem = new QTableWidgetItem(_monitoringWithIntervention);
    } else {
        tableItem = new QTableWidgetItem(_noMonitoring);
    }
    ui->serverTable->setItem(i, c++, tableItem);

    auto hardwareInfo = (co_await api->get(QString("/dedicated/server/%1/specifications/hardware").arg(server), 240h)).object();
    tableItem = new QTableWidgetItem(hardwareInfo["description"].toString());
    ui->serverTable->setItem(i, c++, tableItem);

    // Calculate monthly cost, using the /services API
    auto serviceInfo = (co_await api->get(QString("/dedicated/server/%1/serviceInfos").arg(server), 1h)).object();
    auto serviceId = serviceInfo["serviceId"].toInt();
    auto servicesData = (co_await api->get(QString("/services/%1").arg(serviceId), 6h)).object();
    auto billingData = servicesData["billing"].toObject();
    auto pricingData = billingData["pricing"].toObject();

    double totalCost = pricingData["price"].toObject()["value"].toDouble();
    totalCost = totalCost / pricingData["interval"].toInt();

    tableItem = new QTableWidgetItem(QString::number(totalCost));
    ui->serverTable->setItem(i, c++, tableItem);

    if (billingData["pricing"].isNull()) {
        incompletePricing = true;
    } else {
        qDebug() << pricingData["price"] << pricingData["interval"];
        if (currency.isEmpty())
            currency = pricingData["price"].toObject()["currency"].toString();
        // TODO: else ??
        totalMonthlyCost += totalCost;
    }
    ui->serverTable->resizeColumnsToContents();

    if (incompletePricing) {
        ui->monthlyCost->setText(tr("(incomplete!) %1 %2").arg(totalMonthlyCost).arg(currency));
        ui->monthlyCost->setToolTip(tr("For no good reason, OVH sometimes doesn't provide pricing information, sorry about that"));
    } else {
        ui->monthlyCost->setText(QString("%1 %2").arg(totalMonthlyCost).arg(currency));
    }
}
