#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCoroTask>
#include <QJsonDocument>
#include <QMap>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class QTreeWidgetItem;
class QSignalMapper;
class OvhApi;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionQuit_triggered();

    QCoro::Task<> on_actionLogin_triggered();

    QCoro::Task<> on_serverList_itemClicked(QTreeWidgetItem *, int);

    void on_tabWidget_tabCloseRequested(int index);

    // This is a copy and not a const pointer on purpose
    QCoro::Task<> loadServerInfo(QString serverName);

    void loadKvm(const QString &name, const QString &kvmType, const QString &data);

    QCoro::Task<> login(const QString &ck);
    QCoro::Task<> startKVMRequest(const QString &serverName);

    QCoro::Task<> on_actionGraphPerMonth_triggered();

    QCoro::Task<> on_actionBillsOfLastMonth_triggered();

    QCoro::Task<> on_actionFidelity_triggered();

    void on_actionServices_triggered();

    QCoro::Task<> on_actionPurgeLogins_triggered();

private:
    QAction *addLoginAction(const QString &ck, const QString &display);
    QMap<QString, QTreeWidgetItem*> topLevelNodes;
    QList<QAction *> loginActions;
    QSignalMapper *serverKvmMapper;
    Ui::MainWindow *ui;
    OvhApi *api;
};
#endif // MAINWINDOW_H
