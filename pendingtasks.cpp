#include "pendingtasks.h"
#include "ui_pendingtasks.h"
#include "ovhapi.h"

#include <QTimer>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

PendingTasks::PendingTasks(OvhApi *api, const QString &path, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PendingTasks),
    api(api),
    apiPath(path)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);
    baseRefreshText = ui->refreshLabel->text();
    refreshTimer = new QTimer{this};
    refreshTimer->setInterval(1000);
    refreshTimer->setSingleShot(false);
    refreshTimer->start();
    connect(refreshTimer, &QTimer::timeout, this, &PendingTasks::refresh);
    QTimer::singleShot(0, this, &PendingTasks::refresh);
}

PendingTasks::~PendingTasks()
{
    delete ui;
}

QCoro::Task<> PendingTasks::refresh() {
    auto tasks = co_await api->get(apiPath);
    qDebug() << tasks;
    auto tasksArray = tasks.array();
    ui->tableWidget->setRowCount(tasksArray.count());
    int i = 0;
    for (auto &&taskIdJson: tasksArray) {
        int c = 0;
        auto taskId = taskIdJson.toInt();
        QString taskPath = QString("%1/%2").arg(apiPath).arg(taskId);
        qDebug() << "Fetching on " << taskPath;
        auto taskData = (co_await api->get(taskPath)).object();
        qDebug() << taskData;
        // ID ; Type ; start date ; status
        auto tableItem = new QTableWidgetItem(QString::number(taskId));
        ui->tableWidget->setItem(i, c++, tableItem);
        tableItem = new QTableWidgetItem(taskData["function"].toString());
        ui->tableWidget->setItem(i, c++, tableItem);
        tableItem = new QTableWidgetItem(taskData["startDate"].toString());
        ui->tableWidget->setItem(i, c++, tableItem);
        tableItem = new QTableWidgetItem(taskData["status"].toString());
        if (taskData["status"].toString() == "done")
            tableItem->setToolTip(tr("Done on %1").arg(taskData["doneDate"].toString()));
        ui->tableWidget->setItem(i, c++, tableItem);
        ui->tableWidget->resizeColumnsToContents();
        i++;
    }
    ui->refreshLabel->setText(baseRefreshText.arg(QDateTime::currentDateTime().time().toString()));
}
