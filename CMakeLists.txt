cmake_minimum_required(VERSION 3.5)

project(OvhKvm VERSION 0.1 LANGUAGES CXX)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Network Widgets LinguistTools WebEngineWidgets Sql Charts)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Network Widgets LinguistTools WebEngineWidgets Sql Charts)
find_package(QCoro${QT_VERSION_MAJOR} REQUIRED COMPONENTS Core Network)
find_package(KF${QT_VERSION_MAJOR}Archive REQUIRED)

qcoro_enable_coroutines()

set(TS_FILES OvhKvm_fr_FR.ts)

set(PROJECT_SOURCES
        main.cpp
        mainwindow.cpp
        mainwindow.h
        mainwindow.ui
        dedicatedservergroupwidget.h
        dedicatedservergroupwidget.cpp
        dedicatedservergroupwidget.ui
        dedicatedserverinfowidget.cpp
        dedicatedserverinfowidget.h
        dedicatedserverinfowidget.ui
        ovhwebauthentication.h
        ovhwebauthentication.cpp
        ovhwebauthentication.ui
        ovhapi.cpp
        ovhapi.h
        pendingtasks.h
        pendingtasks.cpp
        pendingtasks.ui
        resources.qrc
        servicesview.h
        servicesview.cpp
        servicesview.ui
        ${TS_FILES}
)

if(${QT_VERSION_MAJOR} GREATER_EQUAL 6)
    qt_add_executable(OvhKvm
        MANUAL_FINALIZATION
        ${PROJECT_SOURCES}
    )
# Define target properties for Android with Qt 6 as:
#    set_property(TARGET OvhKvm APPEND PROPERTY QT_ANDROID_PACKAGE_SOURCE_DIR
#                 ${CMAKE_CURRENT_SOURCE_DIR}/android)
# For more information, see https://doc.qt.io/qt-6/qt-add-executable.html#target-creation

    qt_create_translation(QM_FILES ${CMAKE_SOURCE_DIR} ${TS_FILES})
else()
    if(ANDROID)
        add_library(OvhKvm SHARED
            ${PROJECT_SOURCES}
        )
# Define properties for Android with Qt 5 after find_package() calls as:
#    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android")
    else()
        add_executable(OvhKvm
            ${PROJECT_SOURCES}
        )
    endif()

    qt5_create_translation(QM_FILES ${CMAKE_SOURCE_DIR} ${TS_FILES})
endif()

target_link_libraries(OvhKvm PRIVATE    Qt${QT_VERSION_MAJOR}::Widgets
                                        Qt${QT_VERSION_MAJOR}::Network
                                        Qt${QT_VERSION_MAJOR}::WebEngineWidgets
                                        Qt${QT_VERSION_MAJOR}::Sql
                                        Qt${QT_VERSION_MAJOR}::Charts
                                        QCoro${QT_VERSION_MAJOR}::Core
                                        QCoro${QT_VERSION_MAJOR}::Network
                                        KF${QT_VERSION_MAJOR}::Archive
                                    )

set_target_properties(OvhKvm PROPERTIES
    MACOSX_BUNDLE_GUI_IDENTIFIER my.example.com
    MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION}
    MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}
    MACOSX_BUNDLE TRUE
    WIN32_EXECUTABLE TRUE
)

install(TARGETS OvhKvm
    BUNDLE DESTINATION .
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})

if(QT_VERSION_MAJOR EQUAL 6)
    qt_finalize_executable(OvhKvm)
endif()
