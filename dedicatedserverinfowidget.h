#ifndef DEDICATEDSERVERINFOWIDGET_H
#define DEDICATEDSERVERINFOWIDGET_H

#include <QWidget>

#include <QCoroTask>

namespace Ui {
class DedicatedServerInfoWidget;
}

class OvhApi;

class DedicatedServerInfoWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DedicatedServerInfoWidget(OvhApi *api, const QString &serverName, QWidget *parent = nullptr);
    ~DedicatedServerInfoWidget();

    QString serverName() const { return _serverName; }

signals:
    void kvmRequested();

private slots:
    QCoro::Task<> fetchOvhServerInfo();

    QCoro::Task<> on_findLastBill_clicked();

    void on_kvmButton_clicked();

    void on_renewCgi_clicked();

    QCoro::Task<> on_changeMonitoringButton_clicked();

private:
    Ui::DedicatedServerInfoWidget *ui;
    OvhApi *api;
    const QString _serverName;
};

#endif // DEDICATEDSERVERINFOWIDGET_H
