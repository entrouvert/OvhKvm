<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>DedicatedServerGroupWidget</name>
    <message>
        <location filename="dedicatedservergroupwidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedservergroupwidget.ui" line="22"/>
        <source>Total monthly cost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedservergroupwidget.ui" line="35"/>
        <location filename="dedicatedservergroupwidget.ui" line="53"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedservergroupwidget.ui" line="78"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedservergroupwidget.ui" line="83"/>
        <source>IP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedservergroupwidget.ui" line="88"/>
        <source>Monitoring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedservergroupwidget.ui" line="93"/>
        <source>Hardware</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedservergroupwidget.ui" line="98"/>
        <source>Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedservergroupwidget.cpp" line="26"/>
        <source>Enabled with intervention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedservergroupwidget.cpp" line="27"/>
        <source>Enabled without intervention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedservergroupwidget.cpp" line="28"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedservergroupwidget.cpp" line="39"/>
        <source>Group %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedservergroupwidget.cpp" line="105"/>
        <source>(incomplete!) %1 %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedservergroupwidget.cpp" line="106"/>
        <source>For no good reason, OVH sometimes doesn&apos;t provide pricing information, sorry about that</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DedicatedServerInfoWidget</name>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="20"/>
        <source>General information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="26"/>
        <source>Server name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="39"/>
        <location filename="dedicatedserverinfowidget.ui" line="65"/>
        <location filename="dedicatedserverinfowidget.ui" line="91"/>
        <location filename="dedicatedserverinfowidget.ui" line="123"/>
        <location filename="dedicatedserverinfowidget.ui" line="149"/>
        <location filename="dedicatedserverinfowidget.ui" line="175"/>
        <location filename="dedicatedserverinfowidget.ui" line="210"/>
        <location filename="dedicatedserverinfowidget.ui" line="236"/>
        <location filename="dedicatedserverinfowidget.ui" line="262"/>
        <location filename="dedicatedserverinfowidget.ui" line="288"/>
        <location filename="dedicatedserverinfowidget.ui" line="316"/>
        <location filename="dedicatedserverinfowidget.ui" line="326"/>
        <location filename="dedicatedserverinfowidget.ui" line="357"/>
        <location filename="dedicatedserverinfowidget.ui" line="383"/>
        <location filename="dedicatedserverinfowidget.ui" line="409"/>
        <location filename="dedicatedserverinfowidget.ui" line="435"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="52"/>
        <source>Datacenter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="78"/>
        <source>Rack</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="107"/>
        <source>Calculated on the last month bills.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="110"/>
        <source>Monthly cost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="136"/>
        <source>Creation / expiration date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="162"/>
        <source>Engagement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="191"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="197"/>
        <source>IPv4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="223"/>
        <location filename="dedicatedserverinfowidget.ui" line="275"/>
        <source>Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="249"/>
        <source>IPv6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="301"/>
        <source>OVH Network monitoring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="338"/>
        <source>Hardware information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="344"/>
        <source>CPU</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="370"/>
        <source>RAM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="396"/>
        <source>Storage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="422"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="469"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open renew.cgi for this server only.&lt;/p&gt;&lt;p&gt;Why would you do that while autorenew exists? renew.cgi is by far the simplest way to use your fidelity account on your current servers...&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="472"/>
        <source>Go to renew.cgi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="492"/>
        <source>Find the last bill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.ui" line="512"/>
        <source>Open KVM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.cpp" line="34"/>
        <source>Enabled with intervention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.cpp" line="35"/>
        <source>Enabled without intervention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.cpp" line="36"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.cpp" line="80"/>
        <source>Until %1, &lt;b&gt;automatic reactivation&lt;/b&gt;</source>
        <oldsource>Until %1, &lt;b&gt;automatic reactivation&lt;b&gt;</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.cpp" line="82"/>
        <source>Until %1, stopping engagement after</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.cpp" line="84"/>
        <source>Until %1, unknown strategy %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.cpp" line="96"/>
        <source>&lt;b&gt;Upfront payment for %1 months at once&lt;/b&gt;</source>
        <oldsource>Upfront payment for %1 months at once</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.cpp" line="98"/>
        <source>Periodic payment, engage for %1 months at once</source>
        <oldsource>Periodic payment for %1 months at once</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.cpp" line="100"/>
        <source>Unknown type %1 for %2 months at once</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.cpp" line="104"/>
        <source>No engagement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.cpp" line="135"/>
        <source>IPMI disabled, no KVM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.cpp" line="141"/>
        <source>No KVM available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.cpp" line="155"/>
        <source>Searching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.cpp" line="203"/>
        <source>New monitoring mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dedicatedserverinfowidget.cpp" line="203"/>
        <source>What status do you want for OVH monitoring</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Ovh manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="78"/>
        <source>Ovh KVM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="96"/>
        <source>Billing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="113"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="116"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="133"/>
        <source>Graph per month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="138"/>
        <source>Bills of last month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="143"/>
        <source>Fidelity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="148"/>
        <source>Services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="153"/>
        <source>Purge expired logins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="82"/>
        <source>&amp;Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="125"/>
        <source>&amp;New login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="128"/>
        <source>Ctrl+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="95"/>
        <source>Expired token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="95"/>
        <source>It seems this token is expired. Please login again to continue.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="154"/>
        <source>Validate credential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="154"/>
        <source>Have you validated the OVH credential request?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="210"/>
        <source>Group %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="226"/>
        <source>Info %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="241"/>
        <source>IPMI not activated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="241"/>
        <source>IPMI is not activated on this server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="250"/>
        <source>IPMI KVM not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="250"/>
        <source>IPMI KVM is not available on this server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="269"/>
        <source>Tasks for server %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="297"/>
        <source>KVM %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="306"/>
        <location filename="mainwindow.cpp" line="308"/>
        <source>JNLP KVM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="306"/>
        <source>This server only supports a Java WebStart KVM, it will thus not be embedded in this window.
Java WebStart should be starting right now, wait until it finishes loading to close this dialog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="308"/>
        <source>Failed to launch javaws. Make sure it is properly installed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>Target archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>Zip files (*.zip)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="411"/>
        <source>OVH...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="411"/>
        <source>OVH decided that getting a bill require a &apos;true&apos; login. I&apos;m going to show you a web browser for that.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="422"/>
        <source>Failed to open archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="422"/>
        <source>Failed to open archive...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="514"/>
        <source>Logins cleaned up</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="mainwindow.cpp" line="514"/>
        <source>%n login(s) purged</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>OvhWebAuthentication</name>
    <message>
        <location filename="ovhwebauthentication.ui" line="14"/>
        <source>OVH Web authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ovhwebauthentication.ui" line="29"/>
        <source>Please login on OVH website...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PendingTasks</name>
    <message>
        <location filename="pendingtasks.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pendingtasks.ui" line="21"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pendingtasks.ui" line="26"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pendingtasks.ui" line="31"/>
        <source>Start date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pendingtasks.ui" line="36"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pendingtasks.ui" line="44"/>
        <source>Refreshed on %1</source>
        <oldsource>Refreshing...</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pendingtasks.cpp" line="55"/>
        <source>Done on %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServicesView</name>
    <message>
        <location filename="servicesview.ui" line="14"/>
        <source>Services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="servicesview.ui" line="30"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="servicesview.ui" line="35"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="servicesview.ui" line="40"/>
        <source>Creation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="servicesview.ui" line="45"/>
        <source>Expiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="servicesview.ui" line="50"/>
        <source>Engagement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="servicesview.ui" line="55"/>
        <source>Strategy</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
