#include "ovhwebauthentication.h"
#include "ui_ovhwebauthentication.h"
#include <QTemporaryFile>
#include <QWebEngineProfile>
#include <QCoroSignal>
#include <QDir>
#include <QMessageBox>

OvhWebAuthentication::OvhWebAuthentication(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OvhWebAuthentication)
{
    ui->setupUi(this);
    // Ok, why this fake url ? because OVH limited the target urls, but did not document it in any way...
    ui->webView->setUrl(QUrl{"https://www.ovh.com/auth/?onsuccess=https%3A%2F%2Fwww.ovh.com%2Ffake_url"});
    connect(ui->webView, &QWebEngineView::urlChanged, [this](const QUrl &url) {
        qDebug() << "Started loading url " << url;
        if (url == QUrl("https://www.ovh.com/fake_url")) {
            // SUCCESS !!!!
            ui->webView->setUrl(QUrl{"about:blank"});
            ui->webView->setHtml("<html><body><h1>SUCCESS !</h1></body></html>");
            emit accept();
        }
    });
}

OvhWebAuthentication::~OvhWebAuthentication()
{
    delete ui;
}

QCoro::Task<QByteArray> OvhWebAuthentication::download(const QUrl &url)
{
    QTemporaryFile tempFile;
    if (tempFile.open()) {
        ui->webView->page()->download(url, tempFile.fileName());
        tempFile.close();
        auto webProfile = ui->webView->page()->profile();
        auto dlConn = connect(webProfile, &QWebEngineProfile::downloadRequested, this, [this](QWebEngineDownloadItem *download) {
            qDebug() << "Accepting download";
            download->accept();
            QObject::connect(download, &QWebEngineDownloadItem::finished, this, [this, download]() {
                qDebug() << "Download finished" << download->downloadFileName();
                QDir dlDir{download->downloadDirectory()};
                emit webEngineDownloadFinished(dlDir.absoluteFilePath(download->downloadFileName()));
            });
        });

        const QString dlFileName = co_await qCoro(this, &OvhWebAuthentication::webEngineDownloadFinished);
        qDebug() << "Ok, download finished, understood.";
        disconnect(dlConn);
        QFile dataFile(dlFileName);
        dataFile.open(QIODevice::ReadOnly);
        QByteArray data = dataFile.readAll();
        dataFile.remove();
        co_return data;
    }
    co_return "";
}
