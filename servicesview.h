#ifndef SERVICESVIEW_H
#define SERVICESVIEW_H

#include <QDialog>
#include <QCoroTask>

class OvhApi;
class QAbstractButton;

namespace Ui {
class ServicesView;
}

class ServicesView : public QDialog
{
    Q_OBJECT

public:
    explicit ServicesView(OvhApi *api, QWidget *parent = nullptr);
    ~ServicesView();

private slots:
    QCoro::Task<> loadServices();
    void on_tableWidget_cellChanged(int row, int column);

    QCoro::Task<> on_buttonBox_clicked(QAbstractButton *button);

private:
    Ui::ServicesView *ui;
    OvhApi *api;
    QList <int> changedServices;
};

#endif // SERVICESVIEW_H
