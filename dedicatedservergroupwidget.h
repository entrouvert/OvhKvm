#ifndef DEDICATEDSERVERGROUPWIDGET_H
#define DEDICATEDSERVERGROUPWIDGET_H

#include <QWidget>

#include <QCoroTask>

namespace Ui {
class DedicatedServerGroupWidget;
}

class OvhApi;

class DedicatedServerGroupWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DedicatedServerGroupWidget(OvhApi *api, const QString &groupName, const QStringList &servers, QWidget *parent = nullptr);
    ~DedicatedServerGroupWidget();
    QString groupName() const { return _groupName; }
private slots:
    void fetchOvhData();
    QCoro::Task<> fetchSingleServerData(int row, QString serverName);
signals:
    void requestSingleServerData(int row, QString serverName);
private:
    Ui::DedicatedServerGroupWidget *ui;
    OvhApi *api;
    QString _groupName;
    QStringList _servers;
    double totalMonthlyCost;
    bool incompletePricing;
    QString currency;
};

#endif // DEDICATEDSERVERGROUPWIDGET_H
