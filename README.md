OvhKvm
======

A simple Qt interface for OVH services management.

Why?
----

Because I was tired of waiting for OVH manager to load, especially during emergencies.


Features
----

I implemented what I needed so far:
- browsing your dedicated servers, grouped by reverse domain,
- showing basic server info (name, location, network settings),
- opening a KVM interface,
- getting the last bill for a given server,
- checking/changing server monitoring status,
- revoking service engagement,
- downloading all bills of previous month (directly instead of running the async job on OVH side)

What I plan to add:
- [x] using SQLite to cache OVH API answers. Because it's so incredibly slow and inefficient, esp when searching bills.
- [x] a better interface to get the KVM,
- [x] hardware information
- [x] server monthly cost
- [x] view of pending tasks on a given server (in progress),
- [ ] support for vrack on dedicated servers,
- [ ] a dedicated **searchable** bill view, because it's an ugly mess on OVH side.
- [ ] switch to KDDockWidgets for a nicer interface, maybe

How to build?
----

Easy peasy, mkdir build && pushd build && cmake .. && make -j 42

Dependencies are in debian testing and likely elsewhere, but for fun and learning I decided to use C++20 and coroutines.
It means GCC 10 or Clang 11 minimum.
I test mostly on Qt5 but I sometimes launch it with Qt6 to make sure it remains compatible as much as possible. For the KVM, I had no choice but to use QtWebEngine, sadly. JavaWS KVM are also supported, but only when HTML5 is not available.
And obviously, in order to bind these requirements in the darkness, QCoro, tested with versions 0.8 and 0.9.

So debian dependencies: apt install cmake qttools5-dev qcoro-qt5-dev qtwebengine5-dev libkf5archive-dev

I have not tested this on Windows nor MacOS. I'm more likely to test it on Haiku than on these OSs... As a matter of fact I do, cubbie. It is not flawless sure, but it works and the bugs are mostly in Qt/QtWebEngine or lower level issues.

