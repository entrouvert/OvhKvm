#include "mainwindow.h"

#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include <QSqlDatabase>
#include <QDir>
#include <QStandardPaths>
#include <QSqlQuery>
#include <QSqlError>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("OvhManager");
    a.setOrganizationName("Pinaraf");

    // init db
    QDir shareFolder(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    if (!shareFolder.exists())
        shareFolder.mkpath(shareFolder.absolutePath());
    auto db = QSqlDatabase::addDatabase("QSQLITE");
    QString dbPath = shareFolder.filePath("cache.sqlite");

    db.setDatabaseName(dbPath);
    if (!db.open()) {
        qDebug() << "Failed to open database, check permissions... cache will be disabled!";
        qDebug() << "db file:" << dbPath;
    }

    if (!db.tables().contains("cache")) {
        auto q = db.exec("CREATE TABLE cache(ck character varying(32), path character varying(512) not null, expiration datetime not null, data text not null, PRIMARY KEY(ck, path));");
        if (db.lastError().isValid())
            qDebug() << "Failed to create cache table " << db.lastError().text();
    }

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "OvhKvm_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            a.installTranslator(&translator);
            break;
        }
    }
    MainWindow w;
    w.show();
    return a.exec();
}
