#ifndef OVHAPI_H
#define OVHAPI_H

#include <QObject>
#include <QNetworkRequest>
#include <QCoroTask>
#include <tuple>

using namespace std::chrono_literals;

class QNetworkAccessManager;

class OvhApi : public QObject
{
    Q_OBJECT
public:
    explicit OvhApi(const QString &baseUrl, QObject *parent = nullptr);
    void setConsumerKey(const QString &consumerKey);
public slots:
    QCoro::Task<QJsonDocument> get(const QString &path, std::chrono::seconds cacheDuration = 0s, bool forceRefresh = false);
    QCoro::Task<QJsonDocument> post(const QString &path, const QByteArray &data);
    QCoro::Task<QJsonDocument> put(const QString &path, const QByteArray &data);
    QCoro::Task<> deleteResource(const QString &path);
    QCoro::Task<std::tuple<QString, QUrl>> requestCredentials(const QUrl &callback);

    // Specific helper because sometimes OVH gives external resources...
    QCoro::Task<QByteArray> download(const QUrl &url);
private:
    QCoro::Task<> completeRequest(const QString &method, const QByteArray &data, QNetworkRequest &request, bool auth=true);
    QCoro::Task<> sign(const QString &method, const QByteArray &data, QNetworkRequest &request);
    QCoro::Task<int> time();
    int timeOffset;
    QString consumerKey;
    QNetworkAccessManager *nam;
    QString baseUrl;

signals:

};

#endif // OVHAPI_H
